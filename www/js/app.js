// Ionic Starter App

var db = null;
angular.module('CKC', ['ionic', 'CKC.controllers', 'CKC.services', 'ngCordova', 'ngFileUpload', 'ngStorage'])

  .run(function($ionicPlatform, $cordovaSQLite, $http, $ionicHistory, $rootScope, $cordovaSms, $localStorage, $cordovaNetwork, $rootScope, $timeout, $cordovaSQLite, $q, $window, $location, $httpParamSerializerJQLike, $cordovaNetwork, $state) {

    $ionicPlatform.registerBackButtonAction(function() {
      if ($ionicHistory.currentStateName() === "walkIn" || $ionicHistory.currentStateName() === "survey" || $ionicHistory.currentStateName() === "thanks" || $ionicHistory.currentStateName() === "materialSurvey" || $ionicHistory.currentStateName() === "materialForm") {} else if ($ionicHistory.currentStateName() === "Main") {
        navigator.app.exitApp();
      } else {
        $ionicHistory.goBack();
      }
    }, 100);

    $ionicPlatform.ready(function() {

      // $rootScope.$on('$locationChangeStart',function(event,next,current){
      //   var publicpages = ['/login']
      //   var restrictpages = publicpages.indexOf($location.path())=== -1;
      //   if(restrictpages && (!$localStorage.loginDetails || !$localStorage.loginDetails.isLogin)){
      //     console.log("locations")
      //    $location.path('/login');
      //   }
      // });

      if (window) {
        console.log("i am from browser");
        db = window.openDatabase("my.db", '1.0', 'sqlitedemo', 1024 * 1024 * 100); // browser 
        // db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
        // $cordovaSQLite.execute(db, "DROP TABLE walkindata1");
        // $cordovaSQLite.execute(db, "DELETE FROM adminshop1");
        $cordovaSQLite.execute(db, "CREATE TABLE  adminshop1 (shopid integer primary key UNIQUE,url varchar(50),shopname varchar(25),Description varchar(50),type varchar(50))");
        $cordovaSQLite.execute(db, "CREATE TABLE  materialdata2 (id integer primary key,invoice varchar(50), customer varchar(25),name varchar(25),number varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shopType varchar(50),feedback varchar(50))");
        $cordovaSQLite.execute(db, "CREATE TABLE  walkindata (id integer primary key UNIQUE,customer varchar(25) ,phone varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shoptype varchar(50),feedback varchar(50))");
        $cordovaSQLite.execute(db, "CREATE TABLE  walkindata1 (id integer primary key UNIQUE,customer varchar(25) ,phone varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shoptype varchar(50),feedback json)");
        $cordovaSQLite.execute(db, "CREATE TABLE  feeds (id integer primary key,shopname varchar(50),selectedtype varchar(50),questions varchar(50),selection varchar(50),options json,priority varchar(50))");

      } else {
        console.log("i am mobile")
        db = $cordovaSQLite.openDB({ name: "my.db", location: "default" });
        $cordovaSQLite.execute(db, "CREATE TABLE  adminshop1 (shopid integer primary key UNIQUE,url varchar(50),shopname varchar(25),Description varchar(50),type varchar(50))");
        $cordovaSQLite.execute(db, "CREATE TABLE  materialdata2 (id integer primary key,invoice varchar(50), customer varchar(25),name varchar(25),number varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shopType varchar(50),feedback varchar(50))");
        $cordovaSQLite.execute(db, "CREATE TABLE  walkindata (id integer primary key UNIQUE,customer varchar(25) ,phone varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shoptype varchar(50),feedback varchar(50))");
        $cordovaSQLite.execute(db, "CREATE TABLE  walkindata1 (id integer primary key UNIQUE,customer varchar(25) ,phone varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shoptype varchar(50),feedback json)");
        $cordovaSQLite.execute(db, "CREATE TABLE  feeds (id integer primary key,shopname varchar(50),selectedtype varchar(50),questions varchar(50),selection varchar(50),options json,,priority varchar(50))");
      }

      $http.get('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/GetShopName').then(function(response) {
        console.log(response.data.length)
        angular.forEach(response.data, function(res) {
          console.log(res);
          var query = "INSERT OR REPLACE INTO adminshop1(shopid,url,shopname,Description,type) VALUES (?,?,?,?,?)";
          $cordovaSQLite.execute(db, query, [res.shopid, res.url, res.shopname, res.Description, res.type]).then(function(res) {}, function(err) {});
        })
      });

      $http.get('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/GetFeeds').then(function(response) {
        angular.forEach(response.data, function(res) {
          var query = "INSERT OR REPLACE INTO feeds(id,shopname,selectedtype,questions,selection,options,priority) VALUES (?,?,?,?,?,?,?)";
          $cordovaSQLite.execute(db, query, [res.id, res.shopname, res.selectedtype, res.questions, res.selection, res.options, res.priority]).then(function(res) {}, function(err) {});
        })
      });


      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      if (window.Connection) {
        if (navigator.connection.type == Connection.NONE) {
          console.log("Internet is disconnected on your device");

        } else {
          console.log("Internet working");


          $http.get('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/GetShopName').then(function(response) {
            console.log(response.data.length)
            var query = "SELECT * FROM adminshop1";
            $cordovaSQLite.execute(db, query).then(function(res) {
              console.log(res.rows.length)
              if (response.data.length === res.rows.length) {
                console.log("no udate");
              } else {
                $cordovaSQLite.execute(db, "DELETE FROM adminshop1");
              }
            });
            // if(response.data.length === )
            angular.forEach(response.data, function(res) {
              console.log(res);
              var query = "INSERT OR REPLACE INTO adminshop1(shopid,url,shopname,Description,type) VALUES (?,?,?,?,?)";
              $cordovaSQLite.execute(db, query, [res.shopid, res.url, res.shopname, res.Description, res.type]).then(function(res) {}, function(err) {});
            })
          });


          walkin();

          function walkin() {
            db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
            var query = "SELECT * FROM walkindata1";
            $cordovaSQLite.execute(db, query).then(function(res) {
              console.log()
              var walkindata = [];
              if (res.rows.length >= 1) {
                for (var i = 0; i < res.rows.length; i++) {
                  datas = $httpParamSerializerJQLike({
                    'customer': res.rows.item(i).customer,
                    'phone': res.rows.item(i).phone,
                    'email': res.rows.item(i).email,
                    'address1': res.rows.item(i).address1,
                    'address2': res.rows.item(i).address2,
                    'rm': res.rows.item(i).rm,
                    'shopname': res.rows.item(i).shopname,
                    'shoptype': res.rows.item(i).shoptype,
                    'feed': res.rows.item(i).feedback

                  });
                  var config = {
                    headers: {
                      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                  }

                  var url = 'http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/InsertWalkInData'
                  $http.post(url, datas, config)
                    .success(function(data, status, headers, config) {
                      console.log(data)
                      db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
                      $cordovaSQLite.execute(db, "DROP TABLE walkindata1");
                      $cordovaSQLite.execute(db, "DELETE FROM walkindata1");
                      $cordovaSQLite.execute(db, "CREATE TABLE  walkindata1 (id integer primary key UNIQUE, customer varchar(25) ,phone varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shoptype varchar(50),feedback json)");

                    })

                    .error(function(data, status, header, config) {
                      console.log("error", data)
                    });
                }
                // console.log("walkindata", datas);
              } else {
                // console.log("No results found");
              }
            }, function(err) {
              // console.error("error=>" + err);
            });
          }

          material();
          function material() {
            db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
            var query = "SELECT * FROM materialdata2";
            $cordovaSQLite.execute(db, query).then(function(res) {
              var walkindata = [];
              if (res.rows.length >= 1) {
                for (var i = 0; i < res.rows.length; i++) {
                  datas = $httpParamSerializerJQLike({
                    'invoice': res.rows.item(i).invoice,
                    'customer': res.rows.item(i).customer,
                    'name': res.rows.item(i).name,
                    'number': res.rows.item(i).number,
                    'email': res.rows.item(i).email,
                    'address1': res.rows.item(i).address1,
                    'address2': res.rows.item(i).address2,
                    'rm': res.rows.item(i).rm,
                    'shopname': res.rows.item(i).shopname,
                    'shoptype': res.rows.item(i).shopType,
                    'feedback': res.rows.item(i).feedback
                  });

                  console.log(datas);

                  var config = {
                    headers: {
                      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                  }

                  var url = 'http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/InsertMaterialData'
                  $http.post(url, datas, config)
                    .success(function(data, status, headers, config) {
                      db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
                      $cordovaSQLite.execute(db, "DROP TABLE materialdata2");
                      $cordovaSQLite.execute(db, "DELETE FROM materialdata2");
                      $cordovaSQLite.execute(db, "CREATE TABLE  materialdata2 (id integer primary key,invoice varchar(50), customer varchar(25),name varchar(25),number varchar(25),email varchar(25),address1 varchar(25),address2 varchar(25),rm varchar(25),shopname varchar(50),shopType varchar(50),feedback varchar(50))");

                    })

                    .error(function(data, status, header, config) {});
                }
              } else {}
            }, function(err) {});
          }
        }
      }

    });
  })

  .config(function($stateProvider, $urlRouterProvider) {
    // Each state's controller can be found in controllers.js
    $stateProvider
      // setup an abstract state for the tabs directive
      .state('Main', {
        url: '/Main',
        cache: false,
        templateUrl: 'templates/Main.html',
        controller: 'mainController'
      })

      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginController'
      })

      .state('options', {
        url: '/options',
        templateUrl: 'templates/options.html',
        controller: 'optionsController'
      })

      .state('materialSurvey', {
        url: '/materialSurvey',
        templateUrl: 'templates/materialSurvey.html',
        controller: 'materialSurvey'
      })

      .state('admin', {
        url: '/admin',
        templateUrl: 'templates/admin.html',
        controller: 'adminController',
        resolve: {
          security: ['$q', 'loginservice', '$location', function($q, loginservice, $location) {
            if (loginservice.ckeckpermission()) {
              console.log('allowed');

            } else {
              console.log('Not allowed');
              // alert("not Authorized")
              $location.path('/login');
              // return $q.reject("Not Authorized");
            }
          }]
        }
      })

      .state('walkIn', {
        url: '/walkIn',
        templateUrl: 'templates/walkInForm.html',
        controller: 'walkInController'
      })
      .state('thanks', {
        url: '/thanks',
        templateUrl: 'templates/thanks.html',
        controller: 'thanksController'
      })
      .state('materialForm', {
        url: '/materialForm',
        templateUrl: 'templates/materialised.html',
        controller: 'materialisedController'
      })

      .state('survey', {
        url: '/survey',
        templateUrl: 'templates/survey.html',
        controller: 'surveyController'
      })
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/Main');

  });
