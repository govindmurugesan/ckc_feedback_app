angular.module('CKC.controllers')

  .controller('adminController', function($scope, $rootScope, $state, $ionicModal, $localStorage, $timeout, Upload,$filter, $window, $location, $ionicPopup, $http, $httpParamSerializerJQLike) {
    $scope.doRefresh = function() {
      $cordovaSQLite.execute(db, "DROP TABLE walkindata");
      $cordovaSQLite.execute(db, "DROP FROM adminshop1");
      $cordovaSQLite.execute(db, "DROP TABLE feeds");
      $timeout(function() {
        $cordovaSQLite.execute(db, "DROP TABLE feeds");
        $window.location.reload(true)
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
    };

    $rootScope.showMenu = false;
    $scope.priority = '';

    // Geeting Questions from Server
    getfeedQuestions();

    function getfeedQuestions() {
      $http.get('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/GetFeeds').then(function(response) {
        console.log("response:", response.data);
        $scope.getshopdata = [];
        angular.forEach(response.data, function(res) {
          $scope.getshopdata.push({
            id: res.id,
            shopname: res.shopname,
            selectedtype: res.selectedtype,
            questions: res.questions,
            selection: res.selection,
            options: res.options,
            priority: res.priority
          });
          // $scope.result = res;
          // var query = "INSERT OR REPLACE INTO adminshop1(shopid,url,shopname,Description,type) VALUES (?,?,?,?,?)";
          // $cordovaSQLite.execute(db, query, [res.shopid, res.url, res.shopname, res.Description, res.type]).then(function(res) {
          // });
        });
      });
    }


    // Getting Shopdetails From Server
    getshopdetails();

    function getshopdetails() {
      $http.get('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/GetShopName').then(function(response) {
        console.log("response:", response.data);
        $scope.shopdatatype = [];
        $scope.shopdatatypes = [];
        angular.forEach(response.data, function(res) {
          $scope.shopdatatype.push({
            id: res.shopid,
            url: res.url,
            shopname: res.shopname,
            type: res.type
          });
          $scope.shopdatatypes.push({
            id: res.shopid,
            url: res.url,
            shopname: res.shopname,
            type: res.type
          });
          $scope.choices = ["Input", "Choice"]
          // $scope.Priority = ["Yes", "No"]
          // $scope.priority = "No"
          // $scope.result = res;
          // var query = "INSERT OR REPLACE INTO adminshop1(shopid,url,shopname,Description,type) VALUES (?,?,?,?,?)";
          // $cordovaSQLite.execute(db, query, [res.shopid, res.url, res.shopname, res.Description, res.type]).then(function(res) {
          // });
        });
      });
    }

    getwalkindata()

    function getwalkindata() {
      // Getting Walkin feedback and coustmer data from server
      $http.get('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/GetWalkInData').then(function(response) {
        console.log("response:", response.data);
        $scope.feedbacks = [];
        angular.forEach(response.data, function(res) {
          $scope.feedbacks.push({
            id: res.id,
            customer: res.customer,
            rm: res.rm,
            shopname: res.shopname,
            shoptype: res.shoptype,
            feedback: JSON.parse(res.feedback)
          });
          // $scope.result = res;
          // var query = "INSERT OR REPLACE INTO walkindata (id,customer,phone,email,address1,address2,rm ,shopname,shoptype,feedback) VALUES (?,?,?,?,?,?,?,?,?,?)";
          // $cordovaSQLite.execute(db, query, [res.id, res.customer, res.phone, res.email, res.address1, res.address2, res.rm, res.shopname, res.shoptype, res.feedback]).then(function(res) {}, function(err) {
          //   console.error(err);
          // });
        });

        $scope.shopDetail = [];
        allFeedBack = angular.copy($scope.feedbacks);
        angular.forEach(allFeedBack, function(feeds) {
          $scope.impQuestionDetail = [];
          angular.forEach(feeds.feedback, function(options) {
            // console.log("feeds", feeds.feedback);
            // console.log("options", options);
            if (options.priority && options.priority === "Yes" && (options.ans === "No" || options.ans === "Poor")) {
              $scope.impQuestionDetail.push(options);
            }
          });
          if ($scope.impQuestionDetail.length > 0) {
            feeds.feedback = $scope.impQuestionDetail;
            $scope.shopDetail.push(feeds);
          }
        });

      });
    }
    getmaterialdata();

    function getmaterialdata() {
      // Getting material feedback and Customer data from server
      $http.get('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/GetMaterialData').then(function(response) {
        $scope.materialfeeds = [];
        angular.forEach(response.data, function(res) {
          $scope.materialfeeds.push({
            id: res.id,
            name: res.name,
            rm: res.rm,
            shopname: res.shopname,
            shoptype: res.shoptype,
            feedback: JSON.parse(res.feedback)
          });
          // $scope.result = res;
          // var query = "INSERT OR REPLACE INTO materialdata2 (id,customer,phone,email,address1,address2,rm ,shopname,shoptype,feedback) VALUES (?,?,?,?,?,?,?,?,?,?)";
          // $cordovaSQLite.execute(db, query, [res.id, res.customer, res.phone, res.email, res.address1, res.address2, res.rm, res.shopname, res.shoptype, res.feedback]).then(function(res) {}, function(err) {
          //   console.error(err);
          // });
        });
        $scope.Materialdetails = [];
        allFeedBacks = angular.copy($scope.materialfeeds);
        angular.forEach(allFeedBacks, function(feeds) {
          $scope.impQuestionDetails = [];
          angular.forEach(feeds.feedback, function(options) {
            // console.log("feeds", feeds.feedback);
            // console.log("options", options);
            if (options.priority && options.priority === "Yes" && (options.ans === "No" || options.ans === "Poor")) {
              $scope.impQuestionDetails.push(options);
            }
          });
          if ($scope.impQuestionDetails.length > 0) {
            feeds.feedback = $scope.impQuestionDetails;
            $scope.Materialdetails.push(feeds);
          }
        });
      });

    }


    $scope.data = { url: "", shopname: "", Description: "" };
    selectShop();
    // walkin();
    // fetchdata();
    // material();

    $scope.defaultMenu = function() {
      $scope.expression = true;
      $scope.expression1 = false;
      $scope.expression2 = false;
      $scope.expression3 = false;
      $scope.expression4 = false;
      $scope.expression5 = false;
      $scope.expression6 = false;
      $scope.expression7 = false;
      $scope.expression8 = false;

    }

    $scope.defaultMenu();
    $scope.exp = function() {
      $scope.defaultMenu();
    }

    $scope.exp1 = function() {
      $scope.defaultMenu();
      $scope.expression = false;
      $scope.expression1 = true;
    }

    $scope.exp2 = function() {
      $scope.defaultMenu();
      $scope.expression = false;
      $scope.expression2 = true;
    }
    $scope.exp3 = function() {
      $scope.defaultMenu();
      $scope.expression = false;
      $scope.expression3 = true;
    }

    $scope.exp4 = function() {

      $scope.defaultMenu();
      $scope.expression = false;
      $scope.expression4 = true;
    }

    $scope.exp5 = function() {
      $scope.defaultMenu();
      getwalkindata();
      $scope.expression = false;
      $scope.expression5 = true;
    }

    $scope.exp6 = function() {
      $scope.defaultMenu();
      getmaterialdata();
      $scope.expression = false;
      $scope.expression6 = true;
    }

    $scope.exp7 = function() {
      $scope.defaultMenu();
      getwalkindata()
      $scope.expression = false;
      $scope.expression7 = true;
    }
    $scope.exp8 = function() {
      $scope.defaultMenu();
      getmaterialdata();
      $scope.expression = false;
      $scope.expression8 = true;
    }

    // Adding shop Details to Server
    $scope.adminshop = function(data) {
      if (data.image == null || data.image == "" || data.image == undefined) {
        var alertPopup = $ionicPopup.alert({
          title: 'Enter Shop Details',
          template: 'Please Upload image'
        });
        alertPopup.then(function(res) {});
      } else if (data.shopname == '' || data.shopname == null) {
        var alertPopup = $ionicPopup.alert({
          title: 'Enter Shop Details',
          template: 'Fill All Fields'
        });
        alertPopup.then(function(res) {});
      } else {

        // http://118.67.249.142/CKC_NEW/img/imageName.jpg
        // var full = "http://118.67.249.142/CKC_APP/www/img/"+data.image[0].name;
        var full = "http://118.67.249.142/CKC_NEW/img/" + data.image.name;
        var datas = $httpParamSerializerJQLike({
          shopname: data.shopname,
          url: full,
          Description: data.Description,
          type: ''
        });
        var config = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        }

        $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/InsertShopName', datas, config)
          .success(function(data, status, headers, config) {
            getshopdetails();
            var alertPopup = $ionicPopup.alert({
              title: 'Shop Details',
              template: 'Data Added Successfully'
            });
            alertPopup.then(function(res) {});

          })
          .error(function(data, status, header, config) {});

        var upload;
        upload = Upload.upload({
          url: 'http://118.67.249.142/CKC_NEW/FileUpload.ashx',
          data: { file: data.image }
        });
        console.log(upload);
        console.log(data.image);
        upload.then(function(response) {
          $timeout(function() {});
        }, function(response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function(evt) {});
        $scope.data = { url: "", shopname: "", Description: "" };
      }
    }

    // Adding Shop Type to server
    $scope.admintype = function(data, selectedName, shopname) {
      console.log(data);
      if (selectedName == '' || selectedName == null) {

        var alertPopup = $ionicPopup.alert({
          title: 'Select ShopType',
          template: 'Select Shop type'
        });
        alertPopup.then(function(res) {});
      } else if (data.type == null || data.type == undefined || data.type == "") {
        var alertPopup = $ionicPopup.alert({
          title: 'Enter Data',
          template: 'Fill Input field'
        });
        alertPopup.then(function(res) {});
      } else {
        var data = $httpParamSerializerJQLike({
          'id': selectedName.id,
          'type': data.type
        });
        var config = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        }
        $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/UpdateShopNameType', data, config)
          .success(function(data, status, headers, config) {
            getshopdetails();
            var alertPopup = $ionicPopup.alert({
              title: 'Shop Details',
              template: 'Data Updated Successfully'
            });
            alertPopup.then(function(res) {});
          })
          .error(function(data, status, header, config) {});
        $scope.data = { type: "" };
      }
    }

    $scope.optionstype = [];

    function selectShop() {
      // selectAll('adminshop1').then(function(res) {
      //   var shopName = res;
      //   $scope.shopdata = [];
      //   if (shopName)
      //     for (var i = 0; i < shopName.length; i++) {
      //       $scope.shopdata.push({
      //         id: shopName.item(i).shopid,
      //         url: shopName.item(i).url,
      //         shopname: shopName.item(i).shopname,
      //         Description: shopName.item(i).Description,
      //         type: shopName.item(i).type
      //       });
      //     }
        $scope.typevalue = function(selectedName) {
          if (selectedName === null) {

          } else {
            console.log(selectedName)
            $scope.types = selectedName.type;
            $scope.optionstype = $scope.types.split(",");
          }
        }
      // });
    }

    // function selectAll(tableName) {
    //   var deferred = $q.defer();
    //   db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
    //   var query = "SELECT * FROM " + tableName;
    //   $cordovaSQLite.execute(db, query).then(function(res) {
    //     if (res.rows.length > 0) {
    //       deferred.resolve(res.rows);
    //     } else {
    //       deferred.resolve(false);
    //     }
    //   }, function(err) {});
    //   return deferred.promise;
    // }

    $scope.inputs = [];
    $scope.addfield = function() {
      $scope.inputs.push('');
    }
    $scope.remove = function(item) {
      var lastItem = $scope.inputs.length - 1;
      $scope.inputs.splice(lastItem);
    }
    $scope.radioadd = false;
    $scope.radiooption = function(s) {
      $scope.radioadd = true;
    }

    // Adding Questions to Server
    $scope.add = function(selectedName, selectedtype, data, selection, priority) {
      $scope.lastSubmit = angular.copy($scope.inputs);
      console.log($scope.lastSubmit.length)
      $scope.optionsdata = JSON.stringify($scope.lastSubmit);
      if (selectedName == '' || selectedName == null) {
        var alertPopup = $ionicPopup.alert({
          title: 'Shop Name',
          template: 'select Shop Name'
        });
        alertPopup.then(function(res) {});
      } else if (selectedtype == '' || selectedtype == null) {
        var alertPopup = $ionicPopup.alert({
          title: 'Select ShopType',
          template: 'Select Shop type'
        });
        alertPopup.then(function(res) {});
      } else if (priority == '' || priority == null || priority == undefined) {
        var alertPopup = $ionicPopup.alert({
          title: 'Select Priority',
          template: 'Select Priority option'
        });
        alertPopup.then(function(res) {});
      } else if (selection == '' || selection == null) {
        var alertPopup = $ionicPopup.alert({
          title: 'Select QuestionType',
          template: 'Select Question type'
        });
        alertPopup.then(function(res) {});
      } else if (data.questions == null || data.questions == "") {
        var alertPopup = $ionicPopup.alert({
          title: 'Question',
          template: 'Please Fill the Question '
        });
        alertPopup.then(function(res) {});
      } else if ($scope.optionsdata == null || $scope.optionsdata == "") {
        var alertPopup = $ionicPopup.alert({
          title: 'Question',
          template: 'Please Fill the Question '
        });
        alertPopup.then(function(res) {});
      } else if (selection == 'Choice' && $scope.lastSubmit == "") {
        var alertPopup = $ionicPopup.alert({
          title: 'Question',
          template: 'Please Fill the options '
        });
        alertPopup.then(function(res) {});
      } else {
        var datas = $httpParamSerializerJQLike({
          'shopname': selectedName.shopname,
          'selectedtype': selectedtype,
          'questions': data.questions,
          'selection': selection,
          'options': $scope.optionsdata,
          'priority': priority
        });
        var config = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        }
        $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/InsertFeeds', datas, config)
          .success(function(data, status, headers, config) {
            getfeedQuestions();
            var alertPopup = $ionicPopup.alert({
              title: 'FeedBack Details',
              template: 'Data Addedd Successfully'
            });
            alertPopup.then(function(res) {
              $scope.shopdatatype = []
              getshopdetails();
              $scope.optionstype = { item: '' };
              $scope.choices = { choice: {} };
              $scope.Priority = { priority: '' }
              $scope.hidedata = false;
            });

          })
          .error(function(data, status, header, config) {});

        $scope.radioadd = false;
        $scope.inputs = [];
        data.questions = "";


      }
    }

    $scope.choices = ["Input", "Choice"]
    $scope.qun = true;
$scope.hidedata = false;
    $scope.chage = function(selection) {
      switch (selection) {
        case 'Input':
          {
            $scope.hidedata = true;
            $scope.radioadd = false;
            $scope.Priority = ["No"];
            $scope.priority =  "No"
            $scope.inputs = [];
            break;
          }
        case 'Choice':
          {
            $scope.hidedata = true;
            $scope.radioadd = true;
            $scope.Priority = { priority: '' }
            $scope.Priority = ["No", "Yes"]
            $scope.inputs.push('');
            break
          }
        case 'null':
          break;
        default:
          break;
      }
    }

    $scope.delect = function(item) {
      id = item.id
      var data = $httpParamSerializerJQLike({
        'id': item.id
      });
      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      }
      $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/DeleteFeeds', data, config)
        .success(function(data, status, headers, config) {
          getfeedQuestions();
          var alertPopup = $ionicPopup.alert({
            title: 'FeedBack Details',
            template: 'Data Deleted Successfully'
          });
          alertPopup.then(function(res) {});
        })
        .error(function(data, status, header, config) {});
    }

    function walkin() {
      db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
      var query = "SELECT * FROM walkindata";
      $cordovaSQLite.execute(db, query).then(function(res) {
        $scope.walkindata = [];
        if (res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++) {
            $scope.walkindata.push({
              id: res.rows.item(i).id,
              customer: res.rows.item(i).customer,
              shopname: res.rows.item(i).shopname,
              rm: res.rows.item(i).rm,
              feedback: JSON.parse(res.rows.item(i).feedback),
              phone: res.rows.item(i).phone,
              email: res.rows.item(i).email,
              address: res.rows.item(i).address2,
              shopType: res.rows.item(i).shopType
            });
          }
        } else {}
      }, function(err) {});
    }

    function material() {
      db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
      var query = "SELECT * FROM materialdata2";
      $cordovaSQLite.execute(db, query).then(function(res) {
        $scope.materialdata = [];
        if (res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++) {
            $scope.materialdata.push({
              id: res.rows.item(i).id,
              customer: res.rows.item(i).customer,
              name: res.rows.item(i).name,
              shopname: res.rows.item(i).shopname,
              rm: res.rows.item(i).rm,
              feedback: JSON.parse(res.rows.item(i).feedback),
              email: res.rows.item(i).email,
              address: res.rows.item(i).address2,
              shopType: res.rows.item(i).shopType
            });
          }
        } else {}
      }, function(err) {});
    }

    function fetchdata() {
      db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
      var query = "SELECT * FROM feeds";
      $cordovaSQLite.execute(db, query).then(function(res) {
        $scope.getshopdata = [];
        if (res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++) {
            $scope.getshopdata.push({
              id: res.rows.item(i).id,
              selectedtype: res.rows.item(i).selectedtype,
              shopname: res.rows.item(i).shopname,
              questions: res.rows.item(i).questions,
              selection: res.rows.item(i).selection,
              options: res.rows.item(i).options,
              priority: res.rows.item(i).priority
            });
          }
        } else {}
      }, function(err) {});
    }

    $scope.questions = false;
    $scope.radioquestions = false;
    $scope.question = function() {
      $scope.questions = true;
      $scope.radioquestions = false;
    }

    $scope.radio = function() {
      $scope.radioquestions = true;
      $scope.questions = false;
    }
    $scope.logout = function() {
      $localStorage.loginDetails = '';
      $rootScope.showMenu = false;
      $location.path('login')
      // console.log("logout")
    }

    $scope.home = function() {

      $rootScope.showMenu = false;
      $location.path('Main')
    }

    $scope.shopadmin = function(item) {
      $scope.newdata = [];
      $scope.duplicate = item;
      angular.forEach(JSON.parse($scope.duplicate.options), function(res) {
        $scope.event = res;
        $scope.newdata.push($scope.event);
      })
      $ionicModal.fromTemplateUrl('shopadmin.html', {
        scope: $scope,
      }).then(function(modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    }

    $scope.updatedata = function(data) {
      $scope.newda = JSON.stringify($scope.newdata);
      types = $scope.newda;
      var datas = $httpParamSerializerJQLike({
        'id': data.id,
        'questions': data.questions,
        'options': types,
        'shopname': data.shopname,
        'selectedtype': data.selectedtype,
        'selection': data.selection,
        'priority': data.priority
      });

      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      }
      $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/UpdateFeeds', datas, config)
        .success(function(data, status, headers, config) {})
        .error(function(data, status, header, config) {});
      $scope.modal.remove();
    }

    // fetchshopdata();
    function fetchshopdata() {
      db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
      var query = "SELECT * FROM adminshop1";
      $cordovaSQLite.execute(db, query).then(function(res) {
        $scope.shopdatatype = [];
        if (res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++) {
            $scope.shopdatatype.push({
              id: res.rows.item(i).shopid,
              url: res.rows.item(i).url,
              shopname: res.rows.item(i).shopname,
              type: res.rows.item(i).type,
            });


          }
        } else {}
      }, function(err) {});
    }



    $scope.shoptype = function(item) {
      $scope.newdata = [];
      $scope.duplicate = item;
      $ionicModal.fromTemplateUrl('shopatype.html', {
        scope: $scope,
      }).then(function(modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    }
    $scope.work = {};
    $timeout(function() {
      $scope.work = { pic: 'http://lorempixel.com/100/100/' };
    }, 10);
    $scope.shoptypeupdate = function(duplicate) {
      console.log('duplicate.newUrl', duplicate.newUrl);
      console.log('duplicate.url', duplicate.url);
      if (duplicate.newUrl) {
        full = "http://118.67.249.142/CKC_NEW/img/" + duplicate.newUrl.name;
        duplicate.url = duplicate.newUrl;
      } else
        full = duplicate.url;
      var data = $httpParamSerializerJQLike({
        'id': duplicate.id,
        'shopname': duplicate.shopname,
        'type': duplicate.type,
        'url': full
      });
      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      }
      $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/UpdateShopName', data, config)
        .success(function(data, status, headers, config) {})
        .error(function(data, status, header, config) {});


      var upload;
      upload = Upload.upload({
        url: 'http://118.67.249.142/CKC_NEW/FileUpload.ashx',
        data: { file: duplicate.url }
      });

      upload.then(function(response) {
        $timeout(function() {});
      }, function(response) {
        if (response.status > 0)
          $scope.errorMsg = response.status + ': ' + response.data;
      }, function(evt) {});
      getshopdetails();
      $scope.modal.remove();

    }

    $scope.delectshop = function(item) {
      var data = $httpParamSerializerJQLike({
        'id': item.id
      });
      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      }
      $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/DeleteShopName', data, config)
        .success(function(data, status, headers, config) {
          getshopdetails();

          var alertPopup = $ionicPopup.alert({
            title: 'Shop Details',
            template: 'Data Deleted Successfully'
          });
          alertPopup.then(function(res) {});
        })
        .error(function(data, status, header, config) {});
    }


    $scope.delectWalkIn = function(item) {
      var data = $httpParamSerializerJQLike({
        'id': item.id
      });
      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      }
      $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/DeleteWalkInData', data, config)
        .success(function(data, status, headers, config) {
          getwalkindata()
          var alertPopup = $ionicPopup.alert({
            title: 'WalkInData Details',
            template: 'Data Deleted Successfully'
          });
          alertPopup.then(function(res) {});
        })
        .error(function(data, status, header, config) {});
    }

    $scope.delectMaterial = function(item) {
      console.log(item);
      var data = $httpParamSerializerJQLike({
        'id': item.id
      });
      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      }
      $http.post('http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/DeleteMaterialData', data, config)
        .success(function(data, status, headers, config) {
          getmaterialdata();
          var alertPopup = $ionicPopup.alert({
            title: 'MaterialData Details',
            template: 'Data Deleted Successfully'
          });
          alertPopup.then(function(res) {});
        })
        .error(function(data, status, header, config) {
          //console.log(data);
        });
    }
  });
