angular.module('CKC.controllers')
  .controller('materialSurvey', function($scope, $state, $timeout, $rootScope, $cordovaSQLite, $filter, $ionicPopup, $window, $http, $q, $httpParamSerializerJQLike) {
    console.log("shopname", $rootScope.duplicate.shopname);
    console.log("type", $rootScope.duplicate.type);
    console.log("$rootScope.optionntypeselect:", $rootScope.optionntypeselect)

    var finalfeeds = [];
    $scope.radiooptions = [];
    $scope.radioques = [];
    $scope.selectedtype = {};
    $scope.surveyForm1 = true;
    $scope.surveyForm2 = false;
    $scope.surveyForm3 = false;
    $scope.proceed = true;
    selectAll();

    function selectAll() {
      db = window.openDatabase("my.db", "1.0", "sqlitedemo", 2000);
      var query = "SELECT * FROM feeds";
      $cordovaSQLite.execute(db, query).then(function(res) {
        $scope.shopdata = [];
        if (res.rows.length > 0) {
          console.log("SELECTED -> " + res.rows.item(0).id + " " + res.rows.item(0).s_id);
          for (var i = 0; i < res.rows.length; i++) {
            $scope.shopdata.push({
              id: res.rows.item(i).id,
              questions: res.rows.item(i).questions,
              shopname: res.rows.item(i).shopname,
              selectedtype: res.rows.item(i).selectedtype,
              selection: res.rows.item(i).selection,
              options: res.rows.item(i).options,
              priority: res.rows.item(i).priority
            });
          }
          console.log("dataaaa:", $scope.shopdata);
          $scope.datafiltered = [];
          angular.forEach($scope.shopdata, function(res) {
            console.log('$rootScope.duplicate.shopname == res.shopname', $rootScope.duplicate.shopname == res.shopname);
            if ($rootScope.duplicate.shopname == res.shopname) {
              $scope.datafiltered.push(res);
            }
          })
          console.log('$scope.datafiltered.', $scope.datafiltered);
          console.log("id", $scope.datafiltered)
          $scope.datatype = $filter('filter')($scope.datafiltered, { selectedtype: $rootScope.optionntypeselect });

          console.log('$scope.datatype', $scope.datatype);

          var datatype = angular.copy($scope.datatype);
          // var onlyFour=[];
          $scope.optionsdata = [];
          finalfeeds = [];

          angular.forEach(datatype, function(res, key) {
            console.log(res);
            console.log(key);
            var read = {
              priority: res.priority,
              ans: JSON.parse(res.options),
              qn: res.questions,
              selection: res.selection
            }
            finalfeeds.push(read);
            console.log('finalfeeds', finalfeeds);
          })
          $scope.finalfeeds = finalfeeds.slice(0, 4);
          if (finalfeeds.length <= 4)
            $scope.proceed = false;
          console.log('$scope.finalfeeds', $scope.finalfeeds);
        } else {
          console.log("No results found");
        }
      }, function(err) {
        console.error("error=>" + err);
      });
    }

    $scope.selectAnswers = function(selectedtype) {
      console.log(selectedtype)
    }

    // var count = finalfeeds.length;
    var start = 0,
      end = 4;
    $scope.page1 = function(myform) {
      // count++;
      console.log('page1 myform', myform);
      if (!myform) {
        start += 4;
        end += 4;
        if ((start + 4) >= finalfeeds.length) {
          end = finalfeeds.length;
        }
        console.log('page1 start - %s, end - %s ', start, end);
        $scope.finalfeeds = finalfeeds.slice(start, end);
        console.log('page1 $scope.finalfeeds', $scope.finalfeeds);
        if ((start + 4) >= finalfeeds.length) {
          start = 0;
          end = 0;
          $scope.proceed = false;
          // page3(myform);
        }
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Enter Feedback Details',
          template: 'Please Select all options'
        });
        alertPopup.then(function(res) {});
      }
    }
    $scope.page3 = function(myform2) {
      if (!myform2) {
        var customer = $rootScope.materialFormdatas;
        console.log('customer:', customer);
        console.log($rootScope.walkin)
        var newkeys = [];
        angular.forEach($scope.selectedtype, function(val, key) {
          var fields = val.split('~');
          var feeds = {
            priority: fields[1],
            ques: key,
            ans: fields[0]
          }
          newkeys.push(feeds);
          console.log(feeds);
        });
        $scope.types = JSON.stringify(newkeys);
        var mail = [];
        console.log(newkeys);
        var newdata = [];
        for (var i = 0; i < newkeys.length; i++) {
          if (newkeys[i].priority == "Yes" && (newkeys[i].ans == "No" || newkeys[i].ans == "Poor")) {
            console.log("newkeys[i].priority", newkeys[i]);
            newdata.push(newkeys[i]);
          }
        }

        //  console.log(newdata)
        //  var questionsAndAns = 'Customer Name :' + customer.NAME + '';
        //  angular.forEach(newdata, function(res) {
        //    console.log(res);
        //    questionsAndAns += res.ques + '' + res.ans;
        //  })
        //  questionsAndAns += ''
        //  console.log('questionsAndAns', questionsAndAns);
        //  // var mailss = 'lokesh@ckcsons.com'
        //  var mailss = 'asambasivarao@saptalabs.com,sambasivahai@gmail.com'
        //  var sub = 'materialised Feedback'
        //  datas = $httpParamSerializerJQLike({
        //    'ToEMail': mailss,
        //    'subject': sub,
        //    'body': questionsAndAns
        //  });
        // console.log(datas)
        //  var config = {
        //    headers: {
        //      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        //    }
        //  }


        console.log(newdata)
        var questionsAndAns = '';
        //      questionsAndAns += 'ShopName :' + $rootScope.duplicate.shopname ;
        //        questionsAndAns += 'ShopType :' + $rootScope.optionntypeselect ;

        angular.forEach(newdata, function(res) {
          console.log(res);
          questionsAndAns += res.ques + '' + res.ans + '*'
        })
        questionsAndAns += ''
        console.log('questionsAndAns', questionsAndAns);
        var sub = 'materialised Feedback'
        // var mailss = 'lokesh@ckcsons.com'
        var mailss = 'asambasivarao@saptalabs.com,sambasivahai@gmail.com'
        datas = $httpParamSerializerJQLike({
          'ToEMail': mailss,
          'subject': sub,
          'CustomerName': customer.customer,
          'ShopName': $rootScope.duplicate.shopname,
          'ShopType': $rootScope.optionntypeselect,
          'body2': questionsAndAns
        });

        var config = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        }



        if (newdata.length >= 1) {
          var url = 'http://118.67.249.142/CKC_NEW/ckc_app_Service.asmx/SendMailMessage'
          $http.post(url, datas, config)
            .success(function(data, status, headers, config) {
              console.log(data)

            })
            .error(function(data, status, header, config) {
              console.log("error")
            });
        }
        var query = "UPDATE materialdata2 SET feedback = '" + $scope.types + "'  WHERE id ='" + $rootScope.materialinsertId + "'";
        // var query = "UPDATE adminshop1 SET type=" + types[0] + " WHERE shopname =" + shopname;
        $cordovaSQLite.execute(db, query).then(function(res) {
          console.log("insertId: ", res);
          console.log("ok");


        }, function(err) {
          console.error(err);
        });
        $scope.survey1 = false;
        $scope.survey2 = false;
        $scope.survey3 = false;
        $state.go('thanks');
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Enter Feedback Details',
          template: 'Please Fill all Fields'
        });
        alertPopup.then(function(res) {});
      }
    }

    function hideSurveySection() {
      $scope.surveyForm1 = false;
      $scope.surveyForm2 = false;
      $scope.surveyForm3 = false;
    }
  })
